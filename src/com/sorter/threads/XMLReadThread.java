package com.sorter.threads;

import com.sorter.model.Employee;
import com.sorter.reader.FileReaderInterface;
import com.sorter.reader.XMLReader;

import java.util.*;

public class XMLReadThread extends Thread {

	private List<Employee> list;
	private String fileName;
	
	
	
	public XMLReadThread(String fileName) {
		super();
		this.fileName = fileName;
	}

	public List<Employee> getList() {
		return list;
	}




	public void setList(List<Employee> list) {
		this.list = list;
	}




	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		FileReaderInterface readerObject = new XMLReader();
		list = readerObject.readFile(fileName);
		
	}

}
