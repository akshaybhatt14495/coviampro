package com.sorter.threads;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.sorter.model.Employee;
import com.sorter.reader.CSVReader;
import com.sorter.reader.FileReaderInterface;
import com.sorter.reader.JSONReader;
import com.sorter.reader.TabSeparatedReader;
import com.sorter.reader.XMLReader;
import com.sorter.starter.FileType;

public class ReadThreadCallable implements Callable<List<Employee>> {

	private List<Employee> list;
	private String fileName;
	private FileType fileType;

	public ReadThreadCallable(String fileName, FileType fileType) {
		super();
		this.fileName = fileName;
		this.fileType = fileType;
	}

	public List<Employee> getList() {
		return list;
	}

	public void setList(List<Employee> list) {
		this.list = list;
	}

	private static final Map<FileType, FileReaderInterface> READERS = new HashMap<>();
	static {
		READERS.put(FileType.JSON, new JSONReader());
		READERS.put(FileType.TAB_SEPERATED, new TabSeparatedReader());
		READERS.put(FileType.CSV, new CSVReader());
		READERS.put(FileType.XML, new XMLReader());
	}

	@Override
	public List<Employee> call() throws Exception {
		// TODO Auto-generated method stub
		FileReaderInterface readerObject = READERS.get(fileType);
		return readerObject.readFile(fileName);
	}

	// @Override
	// public List<Employee> call() throws Exception {
	// // TODO Auto-generated method stub
	// if(instanceName.equals("CSV")){
	// FileReaderInterface readerObject = new CSVReader();
	// list = readerObject.readFile(fileName);
	// }else if(instanceName.equals("JSON")){
	// FileReaderInterface readerObject = new JSONReader();
	// list = readerObject.readFile(fileName);
	// }else if(instanceName.equals("TabSeparated")){
	// FileReaderInterface readerObject = new TabSeparatedReader();
	// list = readerObject.readFile(fileName);
	// }else if(instanceName.equals("XML")){
	// FileReaderInterface readerObject = new XMLReader();
	// list = readerObject.readFile(fileName);
	// }
	//
	// //System.out.println(list);
	// return list;
	// }

}
