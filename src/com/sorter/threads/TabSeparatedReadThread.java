package com.sorter.threads;

import java.util.List;

import com.sorter.model.Employee;
import com.sorter.reader.FileReaderInterface;
import com.sorter.reader.TabSeparatedReader;
import com.sorter.reader.XMLReader;

public class TabSeparatedReadThread extends Thread {
	private List<Employee> list;
	private String fileName;
	
	

	public TabSeparatedReadThread(String fileName) {
		super();
		this.fileName = fileName;
	}


	public List<Employee> getList() {
		return list;
	}




	public void setList(List<Employee> list) {
		this.list = list;
	}




	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		FileReaderInterface readerObject = new TabSeparatedReader();
		list = readerObject.readFile(fileName);
		
	}

}
