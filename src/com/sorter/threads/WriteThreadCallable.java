package com.sorter.threads;

import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sorter.model.Employee;
import com.sorter.starter.FileType;
import com.sorter.writer.CSVWriter;
import com.sorter.writer.FileWriterInterface;
import com.sorter.writer.JSONWriter;
import com.sorter.writer.XMLWriter;

public class WriteThreadCallable implements Runnable {

	private Employee employee;
	private FileType fileType;
	private FileWriter fileWriter;

	public WriteThreadCallable(Employee employee, FileType fileType, FileWriter fileWriter) {
		super();
		this.employee = employee;
		this.fileType = fileType;
		this.fileWriter = fileWriter;
	}

	private static final Map<FileType, FileWriterInterface> WRITERS = new HashMap<>();
	static {
		WRITERS.put(FileType.JSON, new JSONWriter());
		WRITERS.put(FileType.CSV, new CSVWriter());
		WRITERS.put(FileType.XML, new XMLWriter());
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		FileWriterInterface writerInterface = WRITERS.get(fileType);
		//System.out.println(fileType);
		
		writerInterface.fileWriter(employee, fileWriter);

	}

}
