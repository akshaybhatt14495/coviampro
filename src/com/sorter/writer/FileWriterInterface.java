package com.sorter.writer;

import java.io.File;
import java.io.FileWriter;

import com.sorter.model.Employee;

public interface FileWriterInterface {
	void fileWriter(Employee employees, FileWriter fileWriter);
	
	FileWriter openWriterFile(String fileName);
	
	void closeWriterFile(FileWriter fileWriter);
}
