/*
 * Created by akshay bhatt
 */
package com.sorter.writer;

import java.io.FileWriter;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.sorter.constants.DataConstants;
import com.sorter.model.Employee;

public class XMLWriter implements FileWriterInterface {

	FileWriter fileWriter = null;

	public XMLWriter(FileWriter fileWriter) {
		super();
		this.fileWriter = fileWriter;
	}

	public XMLWriter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void fileWriter(Employee employee, FileWriter fileWriter) {
		// TODO Auto-generated method stub
		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(Employee.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(employee, fileWriter);
			// jaxbMarshaller.marshal(employee, System.out);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
		

	}

	@Override
	public FileWriter openWriterFile(String fileName) {
		// TODO Auto-generated method stub
		String filePath = DataConstants.WRITE_LOCATION + DataConstants.FILE_NAME + ".xml";
		try {
			fileWriter = new FileWriter(filePath, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fileWriter;
	}

	@Override
	public void closeWriterFile(FileWriter fileWriter) {
		// TODO Auto-generated method stub
		try {
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

/*
 * try { // Creating XML File File file = new File(DataConstants.WRITE_LOCATION
 * + fileName + ".xml");
 * 
 * FileWriter writer = new FileWriter(file);
 * writer.write(DataConstants.XML_COMPANY_TAG); // Loading data in XML File if
 * (!file.exists()) { writer.write(DataConstants.XML_COMPANY_TAG);
 * writer.close(); } // Loading data in XML File
 * 
 * DocumentBuilderFactory documentBuilderFactory =
 * DocumentBuilderFactory.newInstance(); DocumentBuilder documentBuilder =
 * documentBuilderFactory.newDocumentBuilder(); Document document =
 * documentBuilder.parse(DataConstants.WRITE_LOCATION + fileName + ".xml");
 * Element root = document.getDocumentElement();
 * 
 * Element xmlemployee = document.createElement(DataConstants.EMPLOYEE);
 * 
 * Attr attr = document.createAttribute(DataConstants.EMPLOYEE_ID);
 * attr.setValue(Integer.toString(employee.getId()));
 * xmlemployee.setAttributeNode(attr);
 * 
 * Element name = document.createElement(DataConstants.EMPLOYEE_NAME);
 * name.appendChild(document.createTextNode(employee.getName()));
 * xmlemployee.appendChild(name);
 * 
 * Element salary = document.createElement(DataConstants.EMPLOYEE_SALARY);
 * salary.appendChild(document.createTextNode(Integer.toString(employee.
 * getSalary()))); xmlemployee.appendChild(salary);
 * 
 * root.appendChild(xmlemployee);
 * 
 * DOMSource source = new DOMSource(document);
 * 
 * TransformerFactory transformerFactory = TransformerFactory.newInstance();
 * Transformer transformer = transformerFactory.newTransformer(); StreamResult
 * result = new StreamResult(DataConstants.WRITE_LOCATION + fileName + ".xml");
 * transformer.transform(source, result);
 * 
 * } catch (Exception e) { e.printStackTrace(); }
 */
