package com.sorter.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import com.sorter.constants.DataConstants;
import com.sorter.model.Employee;

public class CSVWriter implements FileWriterInterface {

	//BufferedWriter bufferedWriter = null;
	FileWriter fileWriter = null;
	//PrintWriter printWriter = null;
	
	public CSVWriter(FileWriter fileWriter) {
		super();
		//this.bufferedWriter = bufferedWriter;
		this.fileWriter = fileWriter;
		//this.printWriter = printWriter;
	}

	public CSVWriter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void fileWriter(Employee employee, FileWriter fileWriter) {

		final String COMMA_DELIMITER = ",";
		
		try {
			fileWriter.append(String.valueOf(employee.getId()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(employee.getName());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(employee.getSalary()));
			fileWriter.append("\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

	@Override
	public FileWriter openWriterFile(String fileName) {
		// TODO Auto-generated method stub
		String filePath = DataConstants.WRITE_LOCATION + DataConstants.FILE_NAME + ".csv";
		try {
			fileWriter = new FileWriter(filePath, true);
			/*bufferedWriter = new BufferedWriter(fileWriter);
			printWriter = new PrintWriter(bufferedWriter);*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileWriter;
	}

	@Override
	public void closeWriterFile(FileWriter fileWriter) {
		// TODO Auto-generated method stub
		try {
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * public static void main(String args[]) { CSVWriter c = new CSVWriter();
	 * c.fileWriter(new CSVReader().readFile("ReadFiles/Employee.csv"),
	 * "WriteFiles/Employee.csv");
	 * 
	 * }
	 */

}
