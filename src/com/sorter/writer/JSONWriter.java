package com.sorter.writer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

import com.sorter.constants.DataConstants;
import com.sorter.model.Employee;

public class JSONWriter implements FileWriterInterface {

	FileWriter fileWriter = null;
	//PrintWriter printWriter = null;
	
		
    @Override
    public void fileWriter(Employee employee, FileWriter fileWriter) {
    	final String COMMA_DELIMITER = ",";
		
		try {
			fileWriter.append("{\"id\":");
			fileWriter.append(String.valueOf(employee.getId()));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append("{\"name\":");
			fileWriter.append(employee.getName());
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append("{\"salary\":");
			fileWriter.append(String.valueOf(employee.getSalary())+"}");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }


	public JSONWriter(FileWriter fileWriter) {
		super();
		this.fileWriter = fileWriter;
	}

	public JSONWriter() {
		super();
	}


	@Override
	public FileWriter openWriterFile(String fileName) {
		// TODO Auto-generated method stub
		String filePath = DataConstants.WRITE_LOCATION + DataConstants.FILE_NAME + ".csv";
		try {
			fileWriter = new FileWriter(filePath, true);
			/*bufferedWriter = new BufferedWriter(fileWriter);
			printWriter = new PrintWriter(bufferedWriter);*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileWriter;
	}

	@Override
	public void closeWriterFile(FileWriter fileWriter) {
		// TODO Auto-generated method stub
		try {
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}