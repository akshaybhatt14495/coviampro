package com.sorter.xmlParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import org.xml.sax.Attributes;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sorter.model.Employee;

public class EmployeeParserHandler extends DefaultHandler {

	private List<Employee> employeeList = new ArrayList<Employee>();

	private Stack elementStack = new Stack();

	private Stack objectStack = new Stack();

	public void startDocument() throws SAXException {
		// System.out.println("start of the document : ");

	}

	public void endDocument() throws SAXException {
		// System.out.println("end of the document document : ");
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		this.elementStack.push(qName);
		if ("employee".equals(qName)) {

			Employee employee = new Employee();

			if (attributes != null && attributes.getLength() == 1) {
				employee.setId(Integer.parseInt(attributes.getValue(0)));
			}
			this.objectStack.push(employee);

		}
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		this.elementStack.pop();
		if ("employee".equals(qName)) {
			Employee object = (Employee) this.objectStack.pop();
			//System.out.println(object.getName());
			this.employeeList.add(object);
		}
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		String value = new String(ch, start, length).trim();
		if (value.length() == 0) {
			return;
		}

		if ("name".equals(currentElement())) {
			Employee employee = (Employee) this.objectStack.peek();
			employee.setName(value);
		} else if ("salary".equals(currentElement())) {
			Employee employee = (Employee) this.objectStack.peek();
			employee.setSalary(Integer.parseInt(value));
		}
	}

	private String currentElement() {
		return (String) this.elementStack.peek();
	}

	public List<Employee> getEmployees() {
		return employeeList;
	}

}
