/*
 * Created by akshay bhatt
 */
package com.sorter.constants;

public class DataConstants 
{
	public static final String EMPLOYEE_NAME = "name";
	public static final String EMPLOYEE_ID = "id";
	public static final String EMPLOYEE_SALARY = "salary";
	public static final String READ_LOCATION = "ReadFiles/";
	public static final String WRITE_LOCATION = "WriteFiles/";
	public static final String FILE_NAME = "EmployeeData";
	public static final String EMPLOYEE = "employee";
	public static final String COMPANY = "company";
	public static final String XML_COMPANY_TAG = "<company>\n" + 
												"</company>";
}
