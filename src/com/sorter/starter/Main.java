package com.sorter.starter;

public class Main {
	public static void main(String args[]) {
		StarterInterface starter = new Starter();
		starter.readUsingThread();
		//starter.readData();
		starter.sortData();
		//starter.writeData();
		starter.writeUsingThread();
	}

}
