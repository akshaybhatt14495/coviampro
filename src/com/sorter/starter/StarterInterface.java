package com.sorter.starter;

public interface StarterInterface {
	
	public void readData();
	
	public void sortData();
	
	public void writeData();
	
	public void readUsingThread();

	public void writeUsingThread();
}
