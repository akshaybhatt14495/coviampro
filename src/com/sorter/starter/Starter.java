package com.sorter.starter;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.sorter.constants.DataConstants;
import com.sorter.model.Employee;
import com.sorter.reader.CSVReader;
import com.sorter.reader.FileReaderInterface;
import com.sorter.reader.JSONReader;
import com.sorter.reader.TabSeparatedReader;
import com.sorter.reader.XMLReader;
import com.sorter.sort.SortByName;
import com.sorter.sort.SortBySalary;
import com.sorter.sort.Sorter;
import com.sorter.threads.CSVReadThread;
import com.sorter.threads.JSONReadThread;
import com.sorter.threads.ReadThreadCallable;
import com.sorter.threads.TabSeparatedReadThread;
import com.sorter.threads.WriteThreadCallable;
import com.sorter.threads.XMLReadThread;
import com.sorter.writer.CSVWriter;
import com.sorter.writer.FileWriterInterface;
import com.sorter.writer.JSONWriter;
import com.sorter.writer.XMLWriter;
import com.sorter.starter.FileType;

public class Starter implements StarterInterface {

	List<Employee> employeeList = new ArrayList<>();
	List<Employee> sortedList = new ArrayList<>();

	@Override
	public void readData() {

		CSVReadThread csvReadThread = new CSVReadThread(DataConstants.FILE_NAME);
		JSONReadThread jsonReadThread = new JSONReadThread(DataConstants.FILE_NAME);
		XMLReadThread xmlReadThread = new XMLReadThread(DataConstants.FILE_NAME);
		TabSeparatedReadThread tabSeparatedReadThread = new TabSeparatedReadThread(DataConstants.FILE_NAME);

		xmlReadThread.start();
		tabSeparatedReadThread.start();
		csvReadThread.start();
		jsonReadThread.start();

		try {
			xmlReadThread.join();
			tabSeparatedReadThread.join();
			csvReadThread.join();
			jsonReadThread.join();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		employeeList.addAll(xmlReadThread.getList());
		employeeList.addAll(tabSeparatedReadThread.getList());
		employeeList.addAll(csvReadThread.getList());
		employeeList.addAll(jsonReadThread.getList());
		System.out.println(employeeList);
	}

	@Override
	public void sortData() {
		Sorter sorter = null;
		System.out.println("Hello, Press 1 for sorting by name and press 2 for sorting by salary");
		Scanner scan = new Scanner(System.in);
		int choice = scan.nextInt();
		scan.close();
		switch (choice) {
		case 1:
			sorter = new SortByName();
			break;
		case 2:
			sorter = new SortBySalary();
			break;
		default:
			System.out.println("Sorry wrong choice");

		}
		sortedList = sorter.sortEmployee(employeeList);
	}

	@Override
	public void writeData() {
		/*FileWriterInterface writerInterface;

		for (Employee employee : sortedList) {
			writerInterface = new XMLWriter();
			writerInterface.fileWriter(employee, DataConstants.FILE_NAME);
			writerInterface = new CSVWriter();
			writerInterface.fileWriter(employee, DataConstants.FILE_NAME);
			writerInterface = new JSONWriter();
			writerInterface.fileWriter(employee, DataConstants.FILE_NAME);

		}*/

	}

	private static final Map<Integer, FileType> File_Type = new HashMap<>();
	static {
		
		File_Type.put(1, FileType.CSV);
		File_Type.put(2, FileType.XML);
		File_Type.put(3, FileType.JSON);
		File_Type.put(4, FileType.TAB_SEPERATED);
	}

	@Override
	public void readUsingThread() {
		// TODO Auto-generated method stub

		ExecutorService executorService = Executors.newFixedThreadPool(4);
		List<Future<List<Employee>>> resultList = new ArrayList<>();

		for(int i =1; i<5; i++){
			Future<List<Employee>> future = executorService
					.submit(new ReadThreadCallable(DataConstants.FILE_NAME, File_Type.get(i)));
			resultList.add(future);
		}

		for (Future<List<Employee>> listEmployee : resultList) {
			try {
				employeeList.addAll(listEmployee.get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		executorService.shutdown();
		System.out.println("Data read from your files");

	}

	private static final Map<Integer, FileWriter> FILE_WRITER = new HashMap<>();
	static {
		
		FILE_WRITER.put(1, new CSVWriter().openWriterFile(DataConstants.FILE_NAME));
		FILE_WRITER.put(2, new XMLWriter().openWriterFile(DataConstants.FILE_NAME));
		FILE_WRITER.put(3, new JSONWriter().openWriterFile(DataConstants.FILE_NAME));
	}
	
	@Override
	public void writeUsingThread() {
		// TODO Auto-generated method stub
		System.out.println("Please wait writing data ...");
		System.out.println(System.currentTimeMillis());
		
		for (Employee employee : sortedList) {
			ExecutorService executorService = Executors.newFixedThreadPool(3);
			for(int i=1; i<4; i++){
				Runnable runnable = new WriteThreadCallable(employee, File_Type.get(i), FILE_WRITER.get(i));
				executorService.execute(runnable);
			}
			
			executorService.shutdown();

			try {
				  executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
				} catch (InterruptedException e) {
				 e.printStackTrace();
				}

		}
		
		new CSVWriter().closeWriterFile(FILE_WRITER.get(1));
		new XMLWriter().closeWriterFile(FILE_WRITER.get(2));
		new JSONWriter().closeWriterFile(FILE_WRITER.get(3));
		System.out.println(System.currentTimeMillis());
		System.out.println("Sorted data written to your files");

	}

}
