package com.sorter.sort;

import java.util.List;

import com.sorter.model.Employee;

public interface Sorter {
	
	List<Employee> sortEmployee(List<Employee> employee);

}
