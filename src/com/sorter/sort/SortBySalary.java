/*
 * Created by Foram
 */
package com.sorter.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.sorter.model.Employee;

public class SortBySalary implements Sorter {

	@Override
	public List<Employee> sortEmployee(List<Employee> employee) {
		// TODO Auto-generated method stub

		Collections.sort(employee, new Comparator<Employee>() {
			public int compare(Employee e1, Employee e2) {
				return e1.getSalary() - e2.getSalary();
			}

		});

		return employee;
	}

}
