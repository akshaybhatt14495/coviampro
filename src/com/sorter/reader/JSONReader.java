package com.sorter.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import com.sorter.constants.DataConstants;
import com.sorter.model.Employee;


public class JSONReader implements FileReaderInterface {

    @Override
    public List<Employee> readFile(String fileName) {
        List<Employee> empList=new ArrayList<Employee>();
        InputStream input;
    
        try {
            input = new FileInputStream(new File(DataConstants.READ_LOCATION + DataConstants.FILE_NAME + ".json"));
            JsonFactory f=new JsonFactory();
            JsonParser jp=f.createJsonParser(input);
            jp.setCodec(new ObjectMapper());
            jp.nextToken();

            while(jp.hasCurrentToken()){
                Employee token=jp.readValueAs(Employee.class);
               // System.out.println(token.getId()+" "+token.getName());
                empList.add(token);
                jp.nextToken();
            }
            jp.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return empList; 
        
        
    } 
    
}