/*
 * Created by Aishwary
 */
package com.sorter.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import com.sorter.constants.*;
import com.sorter.model.Employee;

public class CSVReader implements FileReaderInterface {

	@SuppressWarnings("resource")
	@Override
	public List<Employee> readFile(String fileName) {
		List<Employee> employeeList = new ArrayList<Employee>();
		BufferedReader br;
		try {
			String filePath = DataConstants.READ_LOCATION + DataConstants.FILE_NAME + ".csv";
			br = new BufferedReader((Reader) new FileReader(new File(filePath)));
			String line;
			while ((line = br.readLine()) != null) {

				String[] entries = line.split(",");
				if (entries.length != 3) {
					System.out.println("bad line: " + line);
				} else {
					if ((entries[0].isEmpty()) || (entries[1].isEmpty()) || (entries[2].isEmpty())) {
						System.out.println("empty field");
					} else {
						Employee employee = new Employee();
						employee.setName(entries[1]);
						try {
							int id = Integer.parseInt(entries[0].trim());
							employee.setId(id);
							try {
								int salary = Integer.parseInt(entries[2].trim());
								employee.setSalary(salary);
							} catch (NumberFormatException e) {
								System.out.println("Salary not in proper format " + entries[2]);
								continue;
							}
						} catch (NumberFormatException e) {
							System.out.println("Id not in proper format " + entries[1]);
							continue;
						}
						employeeList.add(employee);
					}
				}
			}

			/*
			 * for (int index = 0; index < employeeList.size(); index++) {
			 * System.out.println("Name: " + employeeList.get(index).getName());
			 * System.out.println("Salary: " +
			 * employeeList.get(index).getSalary()); System.out.println("Id: " +
			 * employeeList.get(index).getId()); }
			 */
		} catch (IOException e) {

			e.printStackTrace();

		}
		return employeeList;

	}
	
	  public static void main(String args[]) { CSVReader c = new CSVReader();
	  c.readFile("EmployeeData.csv"); }
	 
}
