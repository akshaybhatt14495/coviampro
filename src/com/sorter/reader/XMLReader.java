/*
 * Created by akshay bhatt
 */
package com.sorter.reader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.sorter.model.Employee;
import com.sorter.xmlParser.EmployeeParserHandler;
import com.sorter.constants.DataConstants;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLReaderFactory;

public class XMLReader implements FileReaderInterface {
	@Override
	public List<Employee> readFile(String fileName) {
		List<Employee> employees = new ArrayList<Employee>();
		try {
			EmployeeParserHandler handler = new EmployeeParserHandler();

			org.xml.sax.XMLReader parser = XMLReaderFactory.createXMLReader();

			parser.setContentHandler(handler);

			InputSource fXmlFile = new InputSource(DataConstants.READ_LOCATION + fileName + ".xml");

			parser.parse(fXmlFile);

			employees = handler.getEmployees();

		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}
		for (Employee emp : employees) {
			// System.out.println(emp.getName());
		}
		return employees;
	}

	/*
	 * public static void main(String args[]) { XMLReader x = new XMLReader();
	 * x.readFile("EmployeeData"); }
	 *
	 * try { File fXmlFile = new File(DataConstants.READ_LOCATION + fileName +
	 * ".xml"); DocumentBuilderFactory dbFactory =
	 * DocumentBuilderFactory.newInstance(); DocumentBuilder dBuilder =
	 * dbFactory.newDocumentBuilder(); Document doc = dBuilder.parse(fXmlFile);
	 * doc.getDocumentElement().normalize(); NodeList nList =
	 * doc.getElementsByTagName(DataConstants.EMPLOYEE); List<Employee> datalist
	 * = new ArrayList<>(); for (int temp = 0; temp < nList.getLength(); temp++)
	 * { Node nNode = nList.item(temp); if (nNode.getNodeType() ==
	 * Node.ELEMENT_NODE) { // Getting Attribute of Employee Element eElement =
	 * (Element) nNode; Employee employee = new Employee(); try {
	 * employee.setName(
	 * eElement.getElementsByTagName(DataConstants.EMPLOYEE_NAME).item(0).
	 * getTextContent()); employee.setId(Integer.parseInt(
	 * eElement.getElementsByTagName(DataConstants.EMPLOYEE_SALARY).item(0).
	 * getTextContent())); employee.setSalary(Integer.parseInt(
	 * eElement.getElementsByTagName(DataConstants.EMPLOYEE_SALARY).item(0).
	 * getTextContent())); } catch (Exception e) { e.printStackTrace();
	 * continue; } datalist.add(employee); } } return datalist; } catch
	 * (Exception e) { e.printStackTrace(); return new ArrayList<Employee>(); }
	 */
}
