
package com.sorter.reader;

import java.util.List;

import com.sorter.model.Employee;

public interface FileReaderInterface {
	List<Employee> readFile(String fileName);
}
