/*
 * Created by Foram
 */
package com.sorter.reader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.sorter.constants.DataConstants;
import com.sorter.model.Employee;

public class TabSeparatedReader implements FileReaderInterface {

	private static String path = "ReadFiles/";

	@Override
	public List<Employee> readFile(String fileName) {
		// TODO Auto-generated method stub
		File file = new File(DataConstants.READ_LOCATION + fileName + ".txt");
		Scanner scan = null;
		List<Employee> employees = new ArrayList<>();
		try {
			scan = new Scanner(file);
			while (scan.hasNext()) {
				String currentLine = scan.nextLine();
				String[] splitted = currentLine.split("\t");

				if (splitted.length != 3) {
					System.out.println("bad line: " + currentLine);
				} else {
					if ((splitted[0].isEmpty()) || (splitted[1].isEmpty()) || (splitted[2].isEmpty())) {
						System.out.println("empty field");
					} else {
						Employee employee = new Employee();
						employee.setName(splitted[0].trim());

						try {
							int id = Integer.parseInt(splitted[1].trim());
							employee.setId(id);
							try {
								int salary = Integer.parseInt(splitted[2].trim());
								employee.setSalary(salary);
							} catch (NumberFormatException e) {
								System.out.println("Salary not in proper format " + splitted[2]);
								continue;
							}
						} catch (NumberFormatException e) {
							System.out.println("Id not in proper format " + splitted[1]);
							continue;
						}
						System.out.println(employee);
						employees.add(employee);
					}
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			scan.close();

		}
		return employees;
	}
	/*
	 * public static void main(String args[]){ TabSeparatedReader reader = new
	 * TabSeparatedReader(); reader.readFile("EmployeeData"); }
	 */
}
